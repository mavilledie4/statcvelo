#!/bin/bash

SAVE_DIRECTORY='modified_data/'

# Check that the save directory exists, create it if not
if [ ! -d $SAVE_DIRECTORY ]
then
    mkdir $SAVE_DIRECTORY
fi

ENTRY_DIRECTORY='raw_data'

# Check that the entry directory exists
if [ ! -d $ENTRY_DIRECTORY ]
then
    echo "Attention pas de données en entrée"
    exit 1
fi


# Process data in the given folder
for FILE in $(ls ${ENTRY_DIRECTORY})
do
    # Add folder name
    FILE=${ENTRY_DIRECTORY}/${FILE}

	# Check that the file exists
    if [ ! -e "$FILE" ]
    then
        echo "$FILE n'existe pas"; echo
        continue
    fi


    FILE_WITHOUT_EXTENSION=$(echo $FILE | cut -d "/" -f 2 | cut -d "." -f 1)
    # echo $FILE_WITHOUT_EXTENSION

    echo ${FILE_WITHOUT_EXTENSION}

    # New Input Field Separator
    odlIFS=${IFS}
    IFS=$'\n'

    # Process data every FREQUENCY minutes
    CPT=0
    FREQUENCY=5

    for LIGNE in $(<$FILE)
    do

        if [ $((${CPT} % ${FREQUENCY})) == 0 ]  
        then
            # Delete first field about network 
            LIGNE=$(echo ${LIGNE} | sed -e s/{\"network\".*\"stations\":"\["/{\"stations\":"\["/g)
            LIGNE=$(echo ${LIGNE} | sed -e s/"\]}}"/\]}/g)

            # Delete field : "returning"
            LIGNE=$(echo ${LIGNE} | sed -e s/,\"returning\":[0-9]*/""/g)

            # Delete field : "renting"
            LIGNE=$(echo ${LIGNE} | sed -e s/,\"renting\":[0-9]*/""/g)

            # Delete field : "payment-terminal"
            LIGNE=$(echo ${LIGNE} | sed -e s/,\"payment-terminal\":[a-z]*/""/g)
        
            # Delete field : "last_updated"
            LIGNE=$(echo ${LIGNE} | sed -e s/,\"last_updated\":[0-9]*/""/g)

            # Delete field : "payment"
            LIGNE=$(echo ${LIGNE} | sed -e s/\"payment\":"\["\"key\",\"transitcard\",\"creditcard\",\"phone\""\]",/""/g)

            # Delete field : "extra" and end curly bracket
            LIGNE=$(echo ${LIGNE} | sed -e s/,\"extra\":{/","/g)
            LIGNE=$(echo ${LIGNE} | sed -e s/},\"free_bikes\"/,\"free_bikes\"/g)

            # Make on row for each station (doesn't work with seed cmd)
            # LIGNE=$(echo ${LIGNE} | sed -e s/},{\"/}"\\n"{/g)

            # Delete end comma between stations' information
            # Add $ special symbol after }
            LIGNE=$(echo $LIGNE | sed 's/},/}$/g')

            # Delete stations beginning
            LIGNE=$(echo $LIGNE | sed 's/{"stations":\[//g')

            # Delete last two characters
            for i in $(seq 1 2):
            do
                LIGNE=$(echo $LIGNE | sed '$ s/.$//')
            done

            # Put data in file
            echo $LIGNE >> $SAVE_DIRECTORY/${FILE_WITHOUT_EXTENSION}_modified.json
        
        fi
        CPT=$((${CPT}+1))

    done

done
