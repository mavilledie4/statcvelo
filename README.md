# Satistiques cVelo
Project to get data on state of cVelo network.  

## File  
script : 
- *agentCvelo.sh* to get network cVelo data
- *agentGit.sh* to send data on Gitlab repository
data : 
- *raw_data* contains ndjson files, which are raw data get with API (using agentCvelo.sh)

## Raw data file description
- format name : *raw-yy-mm-dd.ndjson*  
Script is launch at each minute.  
Each line of the file is a json corresponding to the status of all the stations in the cVelo network at a specific moment. (one line per minute)  


## Set the agent schedule
```bash
crontab -e # Launch crontab file

# Add this line at the end of file
* * * * * ~/statcvelo/agentCvelosh
0 */6 * * * ~/statcvelo/agentGitsh
```
