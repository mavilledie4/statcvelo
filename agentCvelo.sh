#!/bin/bash


# API url to get stations Cvelo data
URL_API=http://api.citybik.es/v2/networks/c-velo


# Variable for file name
PATH_FILE=$(pwd)/statcvelo/raw_data
DATA_FILE=${PATH_FILE}/raw
DATA_FILE_EXTENSION=.ndjson

# Get date
CURRENT_DATE=$(date +%y-%m-%d)

# Create File name
CURRENT_FILE=${DATA_FILE}-${CURRENT_DATE}${DATA_FILE_EXTENSION}
# echo ${CURRENT_FILE}

# Get data on API
RAW_ANSWER=$(curl ${URL_API})

# Put Raw data in file
echo ${RAW_ANSWER} >> ${CURRENT_FILE}


# Process data
# ANSWER=${RAW_ANSWER}

# Delete first field about network 
# ANSWER=$(echo ${ANSWER} | sed -e s/{\"network\".*\"stations\":"\["/{\"stations\":"\["/g)
# ANSWER=$(echo ${ANSWER} | sed -e s/"\]}}"/\]}/g)

# Delete field : "returning"
# ANSWER=$(echo ${ANSWER} | sed -e s/,\"returning\":[0-9]*/""/g)

# Delete field : "renting"
# ANSWER=$(echo ${ANSWER} | sed -e s/,\"renting\":[0-9]*/""/g)

# Delete field : "payment"
# ANSWER=$(echo ${ANSWER} | sed -e s/\"payment\":"\["\"key\",\"transitcard\",\"creditcard\",\"phone\""\]",/""/g)

# Delete field : "extra" and end curly bracket
# ANSWER=$(echo ${ANSWER} | sed -e s/,\"extra\":{/","/g)
# ANSWER=$(echo ${ANSWER} | sed -e s/},\"free_bikes\"/,\"free_bikes\"/g)

# Make on row for each station (doesn't work with seed cmd)
# ANSWER=$(echo ${ANSWER} | sed -e s/},{\"/}"\\n"{/g)

# Put data in file
# echo ${ANSWER} >> ${CURRENT_FILE}


